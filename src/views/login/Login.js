import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { authOperations } from '../../state/modules/authentication';
import Validation from '../../helpers/validations';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      emailValid: true,
      passwordValid: true,
    };
  }

  formValidation() {
    if (!this.isEmailValid()) return false;

    if (!this.validatePassword()) return false;

    return true;
  }

  isEmailValid() {
    const isValid = Validation.emailValidation(this.state.email);
    this.setState({
      emailValid: isValid,
    });

    return isValid;
  }

  validatePassword() {
    this.setState({
      passwordValid: !!this.state.password,
    });

    return !!this.state.password;
  }

  handleLoginRequest() {
    if (!this.formValidation()) return;

    const { email, password } = this.state;

    this.props.login({ email, password });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isAuth) {
      this.props.history.push('/todos');
    }
  }

  componentDidMount() {
    if (this.props.isAuth) {
      this.props.history.push('/todos');
    }
  }

  render() {
    return (
      <div className="login__wrapper">
        <h1 className="login__wrapper-title">
          Douglas Pires To-do's Code Demo!
        </h1>
        <div className="login__inner-wrapper">
          <TextField
            className="login__text-field"
            label="User"
            margin="normal"
            variant="outlined"
            onChange={event => this.setState({ email: event.target.value })}
            value={this.state.email}
            error={!this.state.emailValid}
          />
          <TextField
            className="login__text-field"
            label="Password"
            margin="normal"
            variant="outlined"
            type="password"
            onChange={event => this.setState({ password: event.target.value })}
            value={this.state.password}
            error={!this.state.passwordValid}
          />
          <Button
            onClick={() => this.handleLoginRequest()}
            className="login__button"
            variant="contained"
            color="primary"
          >
            Login
          </Button>
          <div className="login__register-anchor-wrapper">
            <Link to="/register">Register</Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { authentication } = state;

  return {
    responseMessage: authentication.responseMessage,
    isAuth: authentication.isAuthenticated,
  };
};

const mapDispatchToProps = {
  login: authOperations.loginAsync,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
