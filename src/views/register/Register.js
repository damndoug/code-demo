import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { authOperations } from '../../state/modules/authentication';

import Validation from '../../helpers/validations';
import { REGISTER_SUCCESSFULL } from '../../helpers/constants';

class Register extends Component {
  constructor() {
    super();

    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      emailValid: true,
      passwordsValid: true,
    };

    this.handleRegisterRequest = this.handleRegisterRequest.bind(this);
  }

  isEmailValid() {
    const isValid = Validation.emailValidation(this.state.email);
    this.setState({
      emailValid: isValid,
    });

    return isValid;
  }

  validatePasswords() {
    const isValid = Validation.passwordValidationMustNotBeEqualOrEmpty(
      this.state.password,
      this.state.confirmPassword,
    );

    this.setState({
      passwordsValid: isValid,
    });

    return isValid;
  }

  formValidation() {
    if (!this.isEmailValid()) return false;

    if (!this.validatePasswords()) return false;

    return true;
  }

  handleRegisterRequest() {
    if (!this.formValidation()) return;

    const { email, password } = this.state;

    this.props.register({ email, password });
  }

  componentDidUpdate() {
    if (this.props.responseMessage === REGISTER_SUCCESSFULL) {
      this.props.history.push('/login');
    }
  }

  render() {
    return (
      <div className="register__wrapper">
        <h1 className="register__wrapper-title">
          Douglas Pires To-do's Code Demo!
        </h1>
        <div className="register__inner-wrapper">
          <TextField
            className="register__text-field"
            label="Email"
            margin="normal"
            variant="outlined"
            onChange={event => this.setState({ email: event.target.value })}
            value={this.state.email}
            error={!this.state.emailValid}
          />
          <TextField
            className="register__text-field"
            label="Password"
            margin="normal"
            type="password"
            variant="outlined"
            onChange={event => this.setState({ password: event.target.value })}
            value={this.state.password}
            error={!this.state.passwordsValid}
          />
          <TextField
            className="register__text-field"
            label="Confirm Password"
            type="password"
            margin="normal"
            variant="outlined"
            onChange={event =>
              this.setState({ confirmPassword: event.target.value })
            }
            value={this.state.confirmPassword}
            error={!this.state.passwordsValid}
          />
          <Button
            onClick={this.handleRegisterRequest}
            className="login__button"
            variant="contained"
            color="primary"
          >
            Register
          </Button>

          <div className="register__login-anchor-wrapper">
            <Link to="/login">Login</Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { authentication } = state;

  return {
    responseMessage: authentication.responseMessage,
  };
};

const mapDispatchToProps = {
  register: authOperations.registerAsync,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);
