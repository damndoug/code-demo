import React, { Component } from 'react';
import Navbar from '../../components/navbar/Navbar';
import { Route, Switch } from 'react-router-dom';
import TodoList from './components/todo-list/TodoList';
import AddTodo from './components/add-todo/AddTodo';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

export default class Todos extends Component {
  constructor() {
    super();

    this.state = {
      open: false,
    };

    this.toggleAddTodo = this.toggleAddTodo.bind(this);
  }

  toggleAddTodo() {
    this.setState({ open: !this.state.open });
  }

  render() {
    return (
      <div>
        <Navbar />
        <Fab
          onClick={this.toggleAddTodo}
          className="todo-add__button"
          color="secondary"
          aria-label="Add"
        >
          <AddIcon />
        </Fab>
        <Switch>
          <Route path="/" component={TodoList} />
        </Switch>
        <AddTodo open={this.state.open} onClose={this.toggleAddTodo} />
      </div>
    );
  }
}
