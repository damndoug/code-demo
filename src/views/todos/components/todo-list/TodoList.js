import React, { Component } from 'react';
import List from '@material-ui/core/List';

import { connect } from 'react-redux';
import { todoOperations } from '../../../../state/modules/todo';
import DefaultTodoListItem from '../../../../components/default-todo-list-item/DefaultTodoListItem';
import { Snackbar, Slide } from '@material-ui/core';
import { EXPIRED_TASKS_MESSAGE } from '../../../../helpers/constants';
import soundFile from '../../../../assets/sounds/notification.mp3';
import NoListCard from './components/NoListCard';

function TransitionUp(props) {
  return <Slide {...props} direction="up" />;
}

class TodoList extends Component {
  constructor() {
    super();
    this.state = {
      todoList: [],
      snackBarOpened: false,
      transition: TransitionUp,
    };
    this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
  }

  componentWillMount() {
    this.props.getTodos();
  }

  removeItem(todo) {
    this.props.removeTodo(todo);
  }

  renderTodoList(todo) {
    return (
      <DefaultTodoListItem
        onRemoveItem={todo => this.removeItem(todo)}
        key={todo.id}
        item={todo}
      />
    );
  }

  handleSnackbarClose() {
    this.setState({
      snackBarOpened: false,
    });
  }

  async handleAudioWarning() {
    const audio = new Audio(soundFile);

    await audio.play();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.todoList && this.props.todoList !== nextProps.todoList) {
      if (nextProps.todoList.some(i => new Date(i.dueDate) < new Date())) {
        this.setState({
          snackBarOpened: true,
        });
        this.handleAudioWarning();
      }
    }
  }

  render() {
    return (
      <div className="todo-list__wrapper">
        <List component="nav">
          {this.props.todoList.length ? (
            this.props.todoList.map(todo => this.renderTodoList(todo))
          ) : (
            <NoListCard />
          )}
        </List>
        <Snackbar
          open={this.state.snackBarOpened}
          onClose={this.handleSnackbarClose}
          TransitionComponent={this.state.transition}
          autoHideDuration={2000}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{EXPIRED_TASKS_MESSAGE}</span>}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    todoList: state.todo.todoList,
  };
};

const mapDispatchToProps = {
  getTodos: todoOperations.getTodoAsync,
  removeTodo: todoOperations.removeTodoAsync,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TodoList);
