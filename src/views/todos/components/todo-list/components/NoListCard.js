import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

export default class NoListCard extends Component {
  render() {
    return (
      <Paper className="no-list__no-item-wrapper" elevation={1}>
        <Typography variant="h5" component="h3">
          You have no To-do.
        </Typography>
        <Typography component="p">
          Please, click on the add button to add a to-do!
        </Typography>
      </Paper>
    );
  }
}
