import React, { Component } from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export default class PriorityGroup extends Component {
  state = {
    value: 'female',
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
    this.props.onPrioritySelection(event.target.value);
  };

  render() {
    return (
      <div className="priority-group__wrapper">
        <FormControl component="fieldset">
          <FormLabel component="legend">Priority</FormLabel>
          <RadioGroup
            aria-label="Priority"
            name="priority"
            value={this.state.value}
            onChange={this.handleChange}
          >
            <FormControlLabel value="high" control={<Radio />} label="High" />
            <FormControlLabel
              value="medium"
              control={<Radio />}
              label="Medium"
            />
            <FormControlLabel value="low" control={<Radio />} label="Low" />
          </RadioGroup>
        </FormControl>
        <small className="priority-group__default-priority-warning-message">
          If no priority is provided, the task will be classified as Medium
        </small>
      </div>
    );
  }
}
