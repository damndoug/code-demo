import React, { Component } from 'react';
import Slide from '@material-ui/core/Slide';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { List } from '@material-ui/core';

import guid from '../../../../helpers/guid';
import PriorityGroup from './components/priority-group/PriorityGroup';
import { todoOperations } from '../../../../state/modules/todo';
import DefaultTodoListItem from '../../../../components/default-todo-list-item/DefaultTodoListItem';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class AddTodo extends Component {
  constructor() {
    super();
    this.state = {
      todoName: "Task's Name",
      dueDate: new Date().toISOString().split('T')[0],
      priority: { id: 1, text: 'medium' },
      todoList: [],
    };

    this.handleClose = this.handleClose.bind(this);
    this.addTodo = this.addTodo.bind(this);
  }

  handleClose() {
    this.props.onClose();
  }

  validateTaskName() {
    if (this.state.todoName) return true;

    return false;
  }

  addTodo() {
    if (!this.validateTaskName()) return;

    this.setState(prevState => ({
      todoList: [
        ...prevState.todoList,
        {
          id: guid(),
          text: this.state.todoName,
          dueDate: this.state.dueDate,
          priority: this.state.priority,
        },
      ],
    }));
  }

  removeTodoFromList(item) {
    const copyTodoList = [...this.state.todoList];

    const idx = copyTodoList.findIndex(i => i.id === item.id);

    copyTodoList.splice(idx, 1);

    this.setState({
      todoList: copyTodoList,
    });
  }

  setDueDate(event) {
    this.setState({
      dueDate: event.target.value,
    });
  }

  setTodoName(event) {
    this.setState({
      todoName: event.target.value,
    });
  }

  renderTodoList(item) {
    return (
      <DefaultTodoListItem
        key={item.id}
        item={item}
        onRemoveItem={() => this.removeTodoFromList(item)}
      />
    );
  }

  prioritySelected(priority) {
    let priorityId;

    switch (priority) {
      case 'high':
        priorityId = 0;
        break;
      case 'medium':
        priorityId = 1;
        break;
      case 'low':
        priorityId = 2;
        break;
      default:
        priorityId = 0;
        break;
    }
    this.setState({
      priority: {
        text: priority,
        id: priorityId,
      },
    });
  }

  handleSave() {
    this.props.sendTodosToDatabase(this.state.todoList);

    this.setState({
      todoList: [],
    });

    this.handleClose();
  }

  render() {
    return (
      <Dialog
        fullScreen
        open={this.props.open}
        onClose={this.handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className="add-todo__app-bar">
          <Toolbar>
            <IconButton
              color="inherit"
              onClick={this.handleClose}
              aria-label="Close"
            >
              <CloseIcon />
            </IconButton>
            <Typography
              variant="h6"
              color="inherit"
              className="add-todo__app-bar-title"
            >
              Add To-do
            </Typography>
            <Button color="inherit" onClick={this.handleSave.bind(this)}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        <div className="add-todo__todos-wrapper">
          <div className="add-todo__input-wrapper">
            <TextField
              className="add-todo__input"
              label="Task's name"
              onChange={this.setTodoName.bind(this)}
              value={this.state.todoName}
              margin="normal"
              required
              error={!this.validateTaskName()}
            />
          </div>

          <div className="add-todo__input-wrapper">
            <TextField
              className="add-todo__date-picker"
              label="Due date"
              type="date"
              onChange={this.setDueDate.bind(this)}
              defaultValue={this.state.dueDate}
              required
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>

          <PriorityGroup
            onPrioritySelection={value => this.prioritySelected(value)}
          />

          <Button
            id="add-todo"
            onClick={this.addTodo}
            variant="contained"
            color="primary"
          >
            Add To-do
          </Button>
        </div>

        <List>
          {this.state.todoList.map(item => this.renderTodoList(item))}
        </List>
      </Dialog>
    );
  }
}

AddTodo.defaultProps = {
  open: false,
};

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  sendTodosToDatabase: todoOperations.addTodoAsync,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddTodo);
