export default class Validation {
  static nameValidation(name) {
    return name.length >= 4;
  }

  static emailValidation(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  static passwordValidationMustNotBeEqualOrEmpty(password, confirmPassword) {
    return (
      password === confirmPassword && password !== '' && confirmPassword !== ''
    );
  }
}
