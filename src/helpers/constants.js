export const REGISTER_SUCCESSFULL = 'User registered successfully';
export const LOGIN_SUCCESSFULL = 'Welcome!';
export const FAREWELL_MESSAGE = 'See you soon! :)';
export const USER_INFO = 'userInfo';
export const NO_LOGGED_USER_MESSAGE = 'No logged user!';
export const EXPIRED_TASKS_MESSAGE = 'There are tasks that are about to expire'