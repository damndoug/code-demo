import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Slide from '@material-ui/core/Slide';
import Snackbar from '@material-ui/core/Snackbar';
import { connect } from 'react-redux';

import Login from './views/login/Login';
import Register from './views/register/Register';
import Todos from './views/todos/Todos';
import { authOperations } from './state/modules/authentication';
import { USER_INFO } from './helpers/constants';
import localstorage from './helpers/localstorage';

function TransitionUp(props) {
  return <Slide {...props} direction="up" />;
}

function PrivateRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        authed === true ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { from: props.location } }}
          />
        )
      }
    />
  );
}
class App extends Component {
  constructor() {
    super();

    this.state = {
      transition: TransitionUp,
      snackBarOpened: false,
      snackMessage: '',
      isAuthenticated: !!localstorage.get(USER_INFO),
    };

    this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
  }

  handleSnackbarClose() {
    this.setState({
      snackBarOpened: false,
      snackMessage: '',
    });
  }

  componentDidMount() {
    this.props.isAuthenticated();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.responseMessage) {
      this.setState({
        snackMessage: nextProps.responseMessage,
        snackBarOpened: true,
      });

      this.props.clearResponseMessage();
    }
  }

  render() {
    return (
      <div className="App">
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <PrivateRoute
            authed={this.props.isAuth}
            path="/todos"
            component={Todos}
          />
          <Redirect from="*" to={this.props.isAuth ? '/todos' : '/login'} />
        </Switch>

        <Snackbar
          open={this.state.snackBarOpened}
          onClose={this.handleSnackbarClose}
          TransitionComponent={this.state.transition}
          autoHideDuration={2000}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{this.state.snackMessage}</span>}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { authentication } = state;

  return {
    responseMessage: authentication.responseMessage,
    isAuth: authentication.isAuthenticated,
  };
};

const mapDispatchToProps = {
  clearResponseMessage: authOperations.clearResponseMessage,
  isAuthenticated: authOperations.isAuthenticated,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false },
)(App);
