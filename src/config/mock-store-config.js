import Adapter from 'enzyme-adapter-react-16';
import createMockStore from 'redux-mock-store';
import { shallow, configure } from 'enzyme';

const initialState = {};

configure({ adapter: new Adapter() });

const shallowWithStore = (component, store = createMockStore(initialState)) => {
  const context = {
    store,
  };
  return shallow(component, { context });
};

export default shallowWithStore;
