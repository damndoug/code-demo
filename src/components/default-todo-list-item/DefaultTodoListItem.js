import React, { Component } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import PropTypes from 'prop-types';

export default class DefaultTodoListItem extends Component {
  handleDateWarning() {
    const { item } = this.props;

    const today = new Date();
    const date = new Date(item.dueDate);
    const timeDiff = date.getTime() - today.getTime();
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    if (diffDays < 0) {
      return 'default-todo__item--danger';
    }

    if (diffDays < 3) {
      return 'default-todo__item--warning';
    }

    return 'default-todo__item--success';
  }

  render() {
    const { item } = this.props;

    return (
      <div key={item.id}>
        <ListItem button className="default-todo__item">
          <span className={this.handleDateWarning()} />
          <ListItemText primary={item.text} secondary={item.priority.text} />
          <ListItemText secondary={item.dueDate}>Due date</ListItemText>
          <ListItemSecondaryAction>
            <IconButton
              onClick={() => this.props.onRemoveItem(item)}
              aria-label="Delete"
            >
              <DeleteIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </div>
    );
  }
}

DefaultTodoListItem.propTypes = {
  item: PropTypes.object,
};
