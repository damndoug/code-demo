import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';

import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { authOperations } from '../../state/modules/authentication';
import SideList from './components/SideList';

class Navbar extends Component {
  constructor() {
    super();
    this.state = {
      openDrawer: false,
      redirect: false,
      redirectToTodos: false,
    };
    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.logout = this.logout.bind(this);
  }

  toggleDrawer() {
    this.setState(prevState => ({
      openDrawer: !prevState.openDrawer,
    }));
  }

  redirectToTodos() {
    if (this.state.redirectToTodos) return <Redirect to="/todos" />;
  }

  logout() {
    this.props.logout();

    this.setState({
      redirect: true,
    });
  }

  render() {
    return (
      <div className="root">
        <AppBar position="static">
          <Toolbar>
            <IconButton
              className="menu-button"
              color="inherit"
              aria-label="Menu"
              onClick={this.toggleDrawer}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className="grow">
              To-dos
            </Typography>
            {this.redirectToTodos()}
            <Button onClick={this.logout} color="inherit">
              LOGOUT
            </Button>
          </Toolbar>
          <Drawer open={this.state.openDrawer} onClose={this.toggleDrawer}>
            <div tabIndex={0} role="button" onKeyDown={this.toggleDrawer}>
              <SideList />
            </div>
          </Drawer>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  logout: authOperations.logoutAsync,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Navbar);
