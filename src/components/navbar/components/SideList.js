import React, { Component } from 'react';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import { Redirect } from 'react-router-dom';

export default class SideList extends Component {
  constructor() {
    super();
    this.renderRedirect = this.renderRedirect.bind(this);
  }
  renderRedirect() {
    return <Redirect to="/login" />;
  }

  render() {
    return (
      <div>
        <List>
          <ListItem>
            <ListItemText primary="Menu" />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem onClick={this.renderRedirect} button>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="List To-dos" />
          </ListItem>
        </List>
      </div>
    );
  }
}
