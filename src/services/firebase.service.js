import { app as firebase, database as firebaseDatabase } from './utils';
import storageHelper from '../helpers/localstorage';
import { USER_INFO, NO_LOGGED_USER_MESSAGE } from '../helpers/constants';

export default class FirebaseService {
  static getTodoList = async callback => {
    let user = await this.getCurrentUser();

    if (!user) {
      user = storageHelper.get(USER_INFO);
    }

    if (!user) return 'Not logged user';

    await firebaseDatabase
      .ref('todos/' + user.uid)
      .on('value', dataSnapshot => {
        let items = [];

        dataSnapshot.forEach(childSnapshot => {
          let item = childSnapshot.val();
          item['key'] = childSnapshot.key;
          items.push(item);
        });

        callback(items);
      });
  };

  static addTodo = async todos => {
    const user = await this.getCurrentUser();

    if (!user) return NO_LOGGED_USER_MESSAGE;

    todos.forEach(
      async todo =>
        await firebaseDatabase.ref(`todos/${user.uid}`).push({ todo }),
    );
  };

  static removeTodo = async todo => {
    const user = await this.getCurrentUser();

    if (!user) return NO_LOGGED_USER_MESSAGE;

    return await firebaseDatabase.ref(`todos/${user.uid}/${todo.key}`).remove();
  };

  static registerNewUser = async (email, password) => {
    try {
      const result = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);

      return result;
    } catch (error) {
      const errorMessage = error.message;

      return errorMessage;
    }
  };

  static getCurrentUser = async () => {
    const user = await firebase.auth().currentUser;

    return user;
  };

  static login = async (email, password) => {
    try {
      const result = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);

      return result;
    } catch (error) {
      const errorMessage = error.message;

      return errorMessage;
    }
  };

  static logout = async () => {
    try {
      const result = await firebase.auth().signOut();

      return result;
    } catch (error) {
      const errorMessage = error.message;

      return errorMessage;
    }
  };
}
