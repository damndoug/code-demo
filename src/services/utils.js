import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firebase-database';

const config = {
  apiKey: 'AIzaSyDT0Gt8k9Ye2ffJkD6Bgnjx0X6h9cH6RVI',
  authDomain: 'todocodedemo.firebaseapp.com',
  databaseURL: 'https://todocodedemo.firebaseio.com',
  projectId: 'todocodedemo',
  storageBucket: '',
  messagingSenderId: '832569509929',
};

const app = firebase.initializeApp(config);
const database = app.database();

export { app, database };
