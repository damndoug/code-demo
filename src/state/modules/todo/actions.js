import { ADD_TODO, GET_TODOS, REMOVE_TODO } from './types';
import FirebaseService from '../../../services/firebase.service';

export const addTodo = (payload = {}) => ({
  type: ADD_TODO,
  payload,
});

export const addTodoAsync = (todos = {}) => {
  return async dispatch => {
    await FirebaseService.addTodo(todos);

    dispatch(addTodo(todos));
  };
};

export const getTodos = (todos = {}) => ({
  type: GET_TODOS,
  todos,
});

export const getTodoAsync = () => {
  return async dispatch => {
    await FirebaseService.getTodoList(todos => {
      const todoList = todos.map(item => {
        item.todo.key = item.key;
        return item.todo;
      });

      dispatch(getTodos(todoList));
    });
  };
};

export const removeTodo = (todo = {}) => ({
  type: REMOVE_TODO,
  todo,
});

export const removeTodoAsync = (todo = {}) => {
  return async _ => {
    await FirebaseService.removeTodo(todo);
  };
};
