import * as types from './types';
import createReducer from '../../utils/createReducer';

const initialState = {
  todoList: [],
};

const todosReducer = createReducer(initialState)({
  [types.ADD_TODO](state, { todos }) {
    return { ...state, todos };
  },

  [types.GET_TODOS](state, { todos }) {
    const sortedTodos = todos.sort((a, b) => a.priority.id - b.priority.id);

    return { ...state, todoList: sortedTodos };
  },
});

export default todosReducer;
