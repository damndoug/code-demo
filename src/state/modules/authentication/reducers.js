import * as types from './types';
import createReducer from '../../utils/createReducer';
import storageHelper from '../../../helpers/localstorage';
import { USER_INFO } from '../../../helpers/constants';
import localstorage from '../../../helpers/localstorage';

const initialState = {
  responseMessage: '',
  userInfo: {},
  isAuthenticated: false,
};

const authReducer = createReducer(initialState)({
  [types.REGISTER_LOGIN_RESPONSE](state, { payload }) {
    return { ...state, responseMessage: payload };
  },

  [types.CLEAR_RESPONSE_MESSAGE](state) {
    return { ...state, responseMessage: '' };
  },

  [types.SET_USER](state, { payload }) {
    storageHelper.set(USER_INFO, payload);

    return { ...state, userInfo: payload };
  },

  [types.IS_AUTHENTICATED](state) {
    return { ...state, isAuthenticated: !!localstorage.get(USER_INFO) };
  },

  [types.LOGOUT](state) {
    storageHelper.remove(USER_INFO);

    return {
      ...state,
      userInfo: {},
      isAuthenticated: false,
    };
  },
});

export default authReducer;
