import {
  REGISTER_LOGIN_RESPONSE,
  CLEAR_RESPONSE_MESSAGE,
  SET_USER,
  LOGOUT,
  IS_AUTHENTICATED,
} from './types';
import FirebaseService from '../../../services/firebase.service';
import {
  REGISTER_SUCCESSFULL,
  LOGIN_SUCCESSFULL,
  FAREWELL_MESSAGE,
} from '../../../helpers/constants';

export const registerLoginResponse = (payload = {}) => {
  return {
    type: REGISTER_LOGIN_RESPONSE,
    payload,
  };
};

export const isAuthenticated = () => ({
  type: IS_AUTHENTICATED,
});

export const setUser = (payload = {}) => ({
  type: SET_USER,
  payload,
});

export const clearResponseMessage = () => {
  return { type: CLEAR_RESPONSE_MESSAGE };
};

export const loginAsync = (payload = {}) => {
  return async dispatch => {
    const { email, password } = payload;

    const response = await FirebaseService.login(email, password);

    const user = await FirebaseService.getCurrentUser();

    if (typeof response === 'object') {
      dispatch(registerLoginResponse(LOGIN_SUCCESSFULL));

      dispatch(setUser(user));

      dispatch(isAuthenticated());
    } else {
      dispatch(registerLoginResponse(response));
    }
  };
};

export const logout = (payload = {}) => ({
  type: LOGOUT,
  payload,
});

export const logoutAsync = (payload = {}) => {
  return async dispatch => {
    await FirebaseService.logout();

    dispatch(logout());

    dispatch(registerLoginResponse(FAREWELL_MESSAGE));
  };
};

export const registerAsync = (payload = {}) => {
  return async dispatch => {
    const { email, password } = payload;

    const response = await FirebaseService.registerNewUser(email, password);

    if (typeof response === 'object') {
      dispatch(registerLoginResponse(REGISTER_SUCCESSFULL));
    } else {
      dispatch(registerLoginResponse(response));
    }
  };
};
