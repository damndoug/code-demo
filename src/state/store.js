import { createStore, combineReducers, applyMiddleware } from 'redux';
import * as reducers from './modules';
import thunk from 'redux-thunk';

export default function configureStore(initialState) {
  const rootReducer = combineReducers(reducers);

  if (initialState) return createStore(rootReducer, applyMiddleware(thunk));

  return createStore(rootReducer, applyMiddleware(thunk));
}
