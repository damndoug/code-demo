import FirebaseService from '../../services/firebase.service';

describe('Authentication tests', () => {
  const email = 'douglas.pires@live.com';

  const password = 'douglas.pires@live.com';

  it('tests authentication through firebase', async () => {
    const user = await FirebaseService.login(email, password);

    expect(user).toHaveProperty('additionalUserInfo');
  });

  it('tests logout through firebase', async () => {
    await FirebaseService.login(email, password);

    const result = await FirebaseService.logout();

    expect(result).toBe(undefined);
  });
});
