import React from 'react';
import * as actions from '../../../state/modules/todo/actions';
import * as types from '../../../state/modules/todo/types';

import Todos from '../../../views/todos/Todos';
import shallowWithStore from '../../../config/mock-store-config';
import AddTodo from '../../../views/todos/components/add-todo/AddTodo';
import { Fab } from '@material-ui/core';
import guid from '../../../helpers/guid';
import todosReducer from '../../../state/modules/todo/reducers';

function getRandomPriority() {
  const priorities = [
    { id: 0, text: 'high' },
    { id: 1, text: 'medium' },
    { id: 2, text: 'low' },
  ];

  return priorities[Math.floor(Math.random() * priorities.length)];
}

export function mockTodoList(howMany) {
  let mocks = [];
  for (let i = 0; i < howMany; i += 1) {
    mocks.push({
      id: guid(),
      text: 'Task name',
      dueDate: new Date().toISOString().split('T')[0],
      priority: getRandomPriority(),
    });
  }
  return mocks;
}

describe('Todos view tests', () => {
  it('should render Todos view correctly', () => {
    const todosView = shallowWithStore(<Todos />);

    expect(todosView).toMatchSnapshot();
  });

  it('should open AddTodo component', () => {
    const todosView = shallowWithStore(<Todos />);

    todosView.find(Fab).simulate('click');

    const addTodo = todosView.find(AddTodo);

    expect(addTodo.length).toBe(1);
    expect(addTodo.props().open).toBe(true);
  });

  it('should create an action to add a todo', () => {
    const payload = {
      id: guid(),
      text: 'Tasks name',
      dueDate: new Date().toISOString().split('T')[0],
      priority: { id: 1, text: 'medium' },
    };

    const expectedAction = {
      type: types.ADD_TODO,
      payload,
    };

    expect(actions.addTodo(payload)).toEqual(expectedAction);
  });

  it('should send todos to reducer and order todos from high to low', () => {
    const todos = mockTodoList(20);
    const expectedAction = {
      type: types.GET_TODOS,
      todos,
    };

    const state = todosReducer({}, actions.getTodos(todos));
    const firstItemText = state.todoList[0].priority.text;
    const lastItemText =
      state.todoList[state.todoList.length - 1].priority.text;

    expect(actions.getTodos(todos)).toEqual(expectedAction);
    expect(firstItemText).toEqual('high');
    expect(lastItemText).toEqual('low');
  });
});
