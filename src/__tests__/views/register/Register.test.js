import React from 'react';

import Register from '../../../views/register/Register';
import shallowWithStore from '../../../config/mock-store-config';

describe('Register view tests', () => {
  it('should render Login view correctly', () => {
    const registerView = shallowWithStore(<Register />);

    expect(registerView).toMatchSnapshot();
  });
});
