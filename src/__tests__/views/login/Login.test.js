import React from 'react';

import Login from '../../../views/login/Login';
import shallowWithStore from '../../../config/mock-store-config';

describe('Login view tests', () => {
  it('should render Login view correctly', () => {
    const loginView = shallowWithStore(<Login />);

    expect(loginView).toMatchSnapshot();
  });
});
