import React from 'react';
import renderer from 'react-test-renderer';
import DefaultTodoItemList from '../../../components/default-todo-list-item/DefaultTodoListItem';
import guid from '../../../helpers/guid';

describe('Unit test for DefaultTodoItemList', () => {
  it('should render correctly DefaultTodoItemList', () => {
    const item = {
      id: guid(),
      text: 'task test',
      priority: {
        id: 0,
        text: 'high',
      },
      dueDate: new Date().toISOString().split('T')[0],
    };

    const todoItemList = renderer
      .create(<DefaultTodoItemList item={item} />)
      .toJSON();

    expect(todoItemList).toMatchSnapshot();
  });
});
