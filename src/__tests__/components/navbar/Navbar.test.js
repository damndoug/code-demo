import React from 'react';
import Navbar from '../../../components/navbar/Navbar';
import shallowWithStore from '../../../config/mock-store-config';

describe('Unit test for Navbar', () => {
  it('should render correctly Navbar', () => {
    const navbar = shallowWithStore(<Navbar />);

    expect(navbar).toMatchSnapshot();
  });
});
