### Stack

- [React](https://reactjs.org/docs/hello-world.html) + [Redux](https://reactjs.org/docs/hello-world.html)
- [Firebase Auth](https://firebase.google.com/docs/auth/);
- [Firebase Realtime Database](https://firebase.google.com/docs/database/)
- [Firebase Hosting environment](https://firebase.google.com/docs/hosting/);
- [Hosted here](https://todocodedemo.firebaseapp.com);

### The Project:

- [x] Has sign-in/Sign-out functionality using [Firebase Auth](https://firebase.google.com/docs/auth/);
- [x] Uses **[Firebase Realtime Database](https://firebase.google.com/docs/database/)** and **Redux** to keep all the TO-DO's;
- [x] Hosted on the _[Firebase Hosting environment](https://firebase.google.com/docs/hosting/)_;
- [x] Assigns priority to a TO-DO and sort them by **highest to lowest priority**;
- [x] Sets a due time. Add real-time visual and auditive hints to the TO-DO item that indicate that the due time is near and has passed;
- [x] Works on Chrome.
- [x] Is responsive (Mobile and Web);
- [x] Has cross-browser support;
- [x] Has Unit tests;
- [x] Has UI following [Material Design concepts](https://material.io/)

## Architecture

The tree view are like below:

```
src
├───assets
├───components
├───config
├───helpers
├───services
├───state
│   ├───modules
│   └───utils
├───styles
├───views
│   ├───login
│   ├───register
│   └───todos
└───__tests__
```

`src/__tests__` is the tests folder.

`src/assets/*` for images, sounds or whatever is not part of the components.

Reusable components are in `src/components/*`.

`src/config/*` for general configurations. In this case I've used to mock a fake store into tests.

`src/helpers/*` folder is for functions that should not be into components and could be reusable in another place in application.

`src/services/*` is where I place the services. In this case, is where I place the `Firebase` API integration to CRUD data.

`src/state/*` is for handle `Redux`. The modules have the following structure:

```
actions.js - Where I handle actions and async calls.
operations.js - Where I import and export actions.
reducers.js - Where I declare reducers.
types.js - Where I declare types.
```

`src/styles/*` is to keep track of the style files that must not be in components folders and where is located the main CSS file compiled by `Stylus`.

`src/views/*` is where are located wrapper components based in what is that view (e.g.: `Login` view has its own wrapper component)

## About Stylus

For this project I've decided to use **[Stylus](http://stylus-lang.com/)** as the css pre-compiler for the sake of its simplicity on writing and easy configuration.

## About components

Each component folder has the following structure:

```
  component-name
  └───components
  └───ComponentName.js
  └───ComponentName.styl
```

Each component may or not have a `components` folder for modularity, when the child component is not reusable and remove the complexity from the wrapper component.

Each component have a `.styl` file, that has to be declared into `main.styl` file into `src/styles` folder.

## About styling components

I've used the **[BEM (Block Element Modifier)](http://getbem.com/introduction/)** methodology for naming classes, so I can know, just at a glance, where that class is from, and is even easier from locating it on Visual Studio Code.

So, you will find this project classes named as `block__element--modifier` style.

For example: `login__input--with-error`.

## About Firebase Hosting

The application is hosted **[here](https://todocodedemo.firebaseapp.com)** using `Firebase Hosting Environment`

Firebase config are in `src/services/utils.js`

## About Tests

For testing I've used mostly **[Jest](https://jestjs.io/)** and **[Enzyime](https://airbnb.io/enzyme/)**.

## Package Manager

For this project I've used **[Yarn](https://yarnpkg.com/)**

##
